import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
	render() {
		//pensando primeiro no desenvolvimento mobile, dispositivos de medio tamanho (expand-md)
		return (
			<div className="container-fluid p-4">
				<nav className="navbar  navbar-expand-md navbar-dark bg-dark fixed-top">

					<a className="navbar-brand" href="#"><i class="fas fa-code"></i>&nbsp;Atlética Binária</a>

					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMainToggler"
						aria-controls="navbarMainToggler" aria-expanded="false" aria-label="Toggle	navigation">
						<span className="navbar-toggler-icon"></span>
					</button>

					<section className="collapse navbar-collapse" id="navbarMainToggler">
						<div className="navbar-nav ml-auto pr-3">
							<a className="nav-item nav-link" href="#">Home</a>
							<a className="nav-item nav-link" href="#">Informações</a>
							<a className="nav-item nav-link" href="#">Login</a>
						</div>
						<form className="form-inline">
							<div className="input-group">
								<div className="input-group-prepend">
									<span className="input-group-text">@</span>
								</div>
								<input type="text" className="form-controls mr-1" placeholder=" Username" />

							</div>
							<button className="btn btn-outline-success ">Login</button>
						</form>

					</section>

				</nav>

				<section id="carouselSection" className="carousel slide" data-ride="carousel" data-interval="3000">
					<ol className="carousel-indicators">
						<li data-target="#carouselSection" data-slide-to="0" className="active"></li>
						<li data-target="#carouselSection" data-slide-to="1"></li>
						<li data-target="#carouselSection" data-slide-to="2"></li>
					</ol>
					<div className="carousel-inner mt-5">
						<div className="carousel-item active">
							<img src="https://i.ibb.co/DVfsLX7/blusaok.jpg"  alt="IMG 1" className="d-block w-100 image-fluid"/>
						</div>
						<div className="carousel-item">
						<img src="https://i.ibb.co/7ngsnJt/img3.jpg"  alt="IMG 2" className="d-block w-100 image-fluid"/>							
						</div>
						<div className="carousel-item">
						<img src="https://i.ibb.co/SRJm9tN/img4.png"  alt="IMG 3" className="d-block w-100 image-fluid"/>
						</div>
					</div>

					<div className="carousel-control-prev" href="#carouselSection" role="button" data-slide="prev">
						<span className="carousel-control-prev-icon" aria-hidden="true"></span>
						<span className="sr-only">Previous</span>
					</div>

					<div className="carousel-control-next" href="#carouselSection" role="button" data-slide="next">
						<span className="carousel-control-next-icon" aria-hidden="true"></span>
						<span className="sr-only">Next</span>
					</div>


					</section>
					<section className="container p-2">
						<ul className="list-unstyled">
							<li className="media pt-5">
								<img src="https://i.ibb.co/FsdbD7G/img2.jpg" height="150" width="150" alt="" className="mr-3" />
								<div className="media-body">
									<h5 className="mt-0 mb-1">Minhas Imagens</h5>
									Atlética Binária.. Conhecendo a atlética!
							</div>
							</li>
							<li className="media pt-2">
								<img src="https://i.ibb.co/GMSXYBH/camisa.jpg" height="150" width="150" alt="" className="mr-3" />
								<div className="media-body">
									<h5 className="mt-0 mb-1">Minhas Imagens</h5>
									Atlética Binária.. Conhecendo a atlética!
							</div>
							</li>
							<li className="media pt-2">
								<img src="https://i.ibb.co/CJG5gCp/img1.jpg" height="150" width="150" alt="" className="mr-3" />
								<div className="media-body">
									<h5 className="mt-0 mb-1">Minhas Imagens</h5>
									Atlética Binária.. Conhecendo a atlética!
							</div>
							</li>
							<li className="media pt-2">
								<img src="https://i.ibb.co/FsdbD7G/img2.jpg" height="150" width="150" alt="" className="mr-3" />
								<div className="media-body">
									<h5 className="mt-0 mb-1">Minhas Imagens</h5>
									Atlética Binária.. Conhecendo a atlética!
							</div>
							</li>
							<li className="media pt-2">
								<img src="https://i.ibb.co/GMSXYBH/camisa.jpg" height="150" width="150" alt="" className="mr-3" />
								<div className="media-body">
									<h5 className="mt-0 mb-1">Minhas Imagens</h5>
									Atlética Binária.. Conhecendo a atlética!
							</div>
							</li>
							<li className="media pt-2">
								<img src="https://i.ibb.co/CJG5gCp/img1.jpg" height="150" width="150" alt="" className="mr-3 mb-5" />
								<div className="media-body">
									<h5 className="mt-0 mb-1">Minhas Imagens</h5>
									Atlética Binária.. Conhecendo a atlética!
							</div>
							</li>



						</ul>
					</section>



				

				<footer className="container col-12 d-flex pt-3 justify-content-around  footer bg-dark text-light text-center">

					<container className="container col-12 col-lg-4 d-flex justify-content-around">
						<i class="fab fa-google-plus-square"></i>
						<i class="fab fa-facebook-square"></i>
						<i class="fab fa-linkedin"></i>
						<i class="fab fa-instagram"></i>
						<i class="fab fa-twitter"></i>
					</container>
				</footer>



			</div>
		);
	}
}

export default App;